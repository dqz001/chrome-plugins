# 2023金秋版：基于Vite4+Vue3的Chrome插件开发教程

基于Vite4的Chrome Extension Manifest V3工程脚手架。

本项目架构实现了以下功能：

- 基于Vite 4.x搭建
- 基于Chrome Extension Manifest V3规范
- 集成Sass/Scss/Less/Stylus
- 集成Element Plus
- 集成mock.js、mockjs-fetch模拟请求
- 集成vue-router
- 将popup、content、background目录互相独立，便于团队协作开发维护
- 按照Chrome Extension最终生成目录要求配置Vite
- 封装fetch，满足popup、content script、background script跨域请求
- 实现了完整的Chrome Extension MV3项目Demo。

# 版本更新记录

上线版本更新

- V1.0.1 初次版本
- V1.0.2 上线审核驳回版本
- V1.0.3 上线审核修改版本
- V1.0.4 新增次数消耗
- V1.0.5 小版本更新（用户唯一性、部分样式错乱问题）
- V1.0.6 UI样式优化



