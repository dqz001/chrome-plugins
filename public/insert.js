// supportedLangs是Element Plus官网的一个js变量

// 寻找最底层节点并执行特定逻辑
function visit(node) {
  let keyword = "Ant Design";

  const regex = new RegExp(`(${keyword})`, "gi");
  const replacement = `<span class="keyword-highlight">$1</span>`;

  if (node.children.length) {
    for (const el of node.children) {
      visit(el);
    }
  } else {
    // 执行逻辑，如添加特定 class 等
    node.classList.add("drop-off-extension");
  }
}

//添加特定样式声明
function addDropOffStyle() {
  const style = document.createElement("style");

  style.setAttribute("data-h", "mime");
  // 添加特定 class 的具体样式声明
  // style.innerHTML = '.drop-off-extension {transform-origin: top left; animation: hinge 2s;} @keyframes hinge { 0% {animation-timing-function: ease-in-out;} 20%,60% {transform: rotate3d(0, 0, 1, 80deg);animation-timing-function: ease-in-out;} 40%,80% {transform: rotate3d(0, 0, 1, 60deg);animation-timing-function: ease-in-out;opacity: 1;} 100% {transform: translate3d(0, 700px, 0);opacity: 0;}}'

  style.innerHTML =
    ".keyword-highlight {background:red;font-size:40px;color:#fff;}";

  document.head.appendChild(style);
  // 动画执行结束后移除特定 class 样式声明
  // setTimeout(() => {
  //     document.head.removeChild(style)
  // }, 6000)
}

function onmouseover(e) {
}

function annotateTextNodes(nodes, keywords) {
  nodes.forEach((node) => {
    // console.log(node)
    if (node.nodeType === Node.TEXT_NODE) {
      for (const keyword of keywords) {
        const regex = new RegExp(`(${keyword})`, "gi");
        const replacement = `<span class="keyword-highlight" id='keyword'>$1<span class='tip'>搜索“$1”</span></span>`;
        const replacedText = node.textContent.replace(regex, replacement);
        if (replacedText !== node.textContent) {
          const newNode = document.createElement("span");
          newNode.innerHTML = replacedText;
          node.replaceWith(newNode);
          break;
        }
      }
    } else if (
      node.nodeType === Node.ELEMENT_NODE &&
      node.nodeName !== "SCRIPT" && node.nodeName !== "CODE" &&
      node.firstChild
    ) {
      annotateTextNodes(node.childNodes, keywords);
    }
  });
}
// annotateTextNodes(document.body.childNodes, ["Ant Design"]);
// addDropOffStyle();
