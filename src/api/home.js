import axios,{apiDomain,getUID,verson} from "./request";

let API_DOMAIN = apiDomain

//获取钱包加密信息
export const walletSearchInfo = (params) => {
  return axios({
      url: `/api/wallet/search/info`,
      method: "get",
      uid:getUID(),
      params
  });
};
//获取插件次数
export const userOperate = (data) => {
  return axios({
      url: `/plugin/${verson}/user/operate`,
      method: "post",
      uid:getUID(),
      data
  });
};

//获取plugin信息
export const pluginInfo = (data) => {
  return axios({
      url: `/plugin/${verson}/info/plugin/list`,
      method: "post",
      uid:getUID(),
      data
  });
};

//点赞、踩
export const reasoningFeedback = (data) => {
  return axios({
      url: `/api/reasoning/reasoning/feedback`,
      method: "POST",
      uid:getUID(),
      data
  });
};

//点赞、踩
export const infiniteQuery = (data) => {
  return axios({
      url: `/search/v2/infinite/url/query`,
      method: "POST",
      data
  });
};

export const infiniteAllQuery = (data) => {
  return axios({
      url: `/search/${verson}/infinite/all/query`,
      method: "POST",
      data
  });
};

export const triggerHomePage = (params) => {
  return axios({
      url: `/api/trigger/home-page`,
      method: "POST",
      params
  });
};

export const searchStreamingAPI = `${API_DOMAIN}/plugin/${verson}/streaming/search`