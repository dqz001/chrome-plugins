import axios from "axios";
import { ElLoading, ElMessage } from "element-plus";
import { useRouter } from 'vue-router'
// let API_DOMAIN = ''
let VERSION = 'V3'
let API_DOMAIN = 'https://api-test.a.site'
// 请求服务器地址（正式build环境真实请求地址）
// if (import.meta.env.MODE === 'production') {
//     API_DOMAIN = 'https://api.a.site'
// }
 
//创建axios的一个实例
let instance = axios.create({
    baseURL: API_DOMAIN, //接口统一域名
    timeout: 60000, //设置超时
    headers: {
        "Content-Type": "application/json;charset=UTF-8;",
    },
});
let loading;
//正在请求的数量
let requestCount = 0;
//显示loading
const showLoading = () => {
    if (requestCount === 0 && !loading) {
        loading = ElLoading.service({
            text: "Loading  ",
            // background: 'rgba(0, 0, 0, 0.7)',
            spinner: "el-icon-loading",
        });
    }
    requestCount++;
};
//隐藏loading
const hideLoading = () => {
    requestCount--;
    if (requestCount == 0) {
        loading.close();
    }
};

let timestamp = new Date().getTime();
let random = Math.floor(Math.random() * 1000000);
let uid = null

// 获取数据
async function asyncGetData(){
  if(chrome&&chrome.storage&&chrome.storage.sync){
    const data = await chrome.storage.sync.get(['uid']);
    if(data&&data.uid){
      uid = data.uid
    }else{
      uid = timestamp.toString() + random.toString();
      await chrome.storage.sync.set({ uid: uid })
    }
  }else{
    uid = timestamp.toString() + random.toString();
  }
}
asyncGetData()

//请求拦截器
instance.interceptors.request.use(
    (config) => {
        //若请求方式为post，则将data参数转为JSON字符串
          if(uid&&config.url ||config.uid&&config.url ){
            config.url = config.url.indexOf("?")!=-1 ? `${config.url}&uid=${uid}` :  `${config.url}?uid=${uid}`
          }
        if (config.method === "POST") {
            config.data = JSON.stringify(config.data);
        }
        return config;
    },
    (error) =>
    // 对Request error做些什么
    Promise.reject(error)
);

//响应拦截器
instance.interceptors.response.use(
  (response) => {
        // hideLoading()
        //响应成功
        return response.data;
    },
    (error) => {
        //响应错误
        if (error.response && error.response.status) {
            const status = error.response.status;
            let message = "";
            switch (status) {
                case 400:
                    message = "Request error";
                    break;
                case 401:
                    message = "Request error";
                    break;
                case 403:
                  message = "Request error";
                  break;
                case 404:
                    message = "Request address error";
                    break;
                case 408:
                    message = "Request timeout";
                    break;
                case 500:
                    message = "Server internal error!";
                    break;
                case 501:
                    message = "Service not implemented!";
                    break;
                case 502:
                    message = "Bad Gateway!";
                    break;
                case 503:
                    message = "Service unavailable!";
                    break;
                case 504:
                    message = "gateway timeout!";
                    break;
                case 505:
                    message = "HTTP version is not supported";
                    break;
                case 2002:
                  // location.href = location.origin 
                  message = "Request error";
                  break;
                default:
                    message = "Request was aborted";
            }
            ElMessage.error(message);
            return Promise.reject(error);
        }
        return Promise.reject(error);
    }
);

export const getUID = function(){
  return uid
}
export const apiDomain = API_DOMAIN

export const verson = VERSION

export default instance;

