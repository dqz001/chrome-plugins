//    "default_popup": "index.html",/*global chrome*/
import  { apiRequest,apiReqs,APIDOMAIN } from '@/api'

let data = []
chrome.runtime.onInstalled.addListener(function () {
  keywordsAll()
})

chrome.runtime.onMessage.addListener(function (request, sender, sendResponse) {
    // 接收来自content script的消息，requset里不允许传递function和file类型的参数
    chrome.tabs.query({ currentWindow: true, active: true }, function (tabs) {
        const { contentRequest,msg } = request
        // 接收来自content的api请求
        if (contentRequest === 'apiRequest') {
            let { config } = request
            // API请求成功的回调
            config.success = (data) => {
                data.result = 'succ'
                sendResponse(data)
            }
            // API请求失败的回调
            config.fail = (msg) => {
                sendResponse({
                    result: 'fail',
                    msg,
                })
            }
            // 发起请求
            apiRequest(config)
        }

        if (msg === 'GETDATA') {
            // chrome.runtime.reload() //重载Chrome插件
            sendResponse(data) //回传消息给content_scripts
        }
    })
    return true
})
let catchInfo ={}
// // 监听标签页更新事件
chrome.tabs.onUpdated.addListener(function(tabId, changeInfo, tab) {
  // 检查链接是否发生变化
  if(changeInfo.status=='loading'){
    catchInfo = changeInfo
  }
  if (changeInfo.status=='complete'&&catchInfo.url && catchInfo.url.startsWith('https://www.baidu.com')) {
    chrome.tabs.reload(tabId);
  }
});
//
chrome.webRequest.onCompleted.addListener(
    function(details) {
      console.log(details)
        chrome.tabs.sendMessage(details.tabId, { action: 'ReLoaded',data:data,details:details} );
    },
    //<all_urls>
    {urls: ["*://www.google.com.hk/search?*","*://twitter.com/i/api/graphql/*","*://www.google.com/search?*","*://etherscan.io/*"]}
);  

// 监听网页加载完成事件
chrome.webNavigation.onCompleted.addListener(function(details) {
  keywordsAll()
  chrome.tabs.sendMessage(details.tabId, { action: 'pageLoaded',data:data ,details} );
}, { url: [{ schemes: ['http', 'https'] }] }); // 只监听http和https协议的网页加载完成事件

async function keywordsAll(){
  if(data&&data.length>0){
    if(chrome&&chrome.storage&&chrome.storage.sync){
      const catcData = await chrome.storage.sync.get(['cacheQueryDate']);
      if(catcData&&catcData.cacheQueryDate){
        if(catcData.cacheQueryDate!=getTime()){
          fetchInfiniteAllQuery()
        }
      }else{
        await chrome.storage.sync.set({ cacheQueryDate: getTime() })
      }
    }
  }else{
    fetchInfiniteAllQuery()
  }
}

function fetchInfiniteAllQuery(){
  fetch(APIDOMAIN + '/search/v2/infinite/all/query', {method:'post'})
    .then((res) => res.json())
    .then((result) => {
        data = result
        if(chrome&&chrome.storage&&chrome.storage.sync){
            chrome.storage.sync.set({ cacheQueryDate: getTime() })
        }
    })
    .catch(() => {
    })
}

// 格式化日对象
function getTime() {
  let date = new Date(); //时间戳为10位需*1000，时间戳为13位的话不需乘1000
  let Y = date.getFullYear()
  let  M =date.getMonth() + 1 < 10 ? '0' + (date.getMonth() + 1) : date.getMonth() + 1
  let  D = date.getDate() < 10 ? '0' + date.getDate() : date.getDate()
  return Y + '-' + M + '-' + D ;
}