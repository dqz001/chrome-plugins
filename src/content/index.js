import { createApp } from "vue";
import ElementPlus from "element-plus";
import 'element-plus/dist/index.css'
import zhCn from "element-plus/dist/locale/zh-cn.mjs";
import Content from "@/content/content.vue";
import { apiReqs } from "@/api";
import store from "../store";
import router from './router'
import i18n from '@/locale/index'

let timer = null
// 创建id为adot-container的div
const crxApp = document.createElement("div");
crxApp.id = "adot-container";
// 将刚创建的div插入body最后
document.body.appendChild(crxApp);
// 创建Vue APP
const app = createApp(Content);
app.use(ElementPlus, {
  locale: zhCn,
})
app.use(i18n)
app.use(store);
app.use(router)
app.mount("#adot-container");

let myTimer = null 
let num = 0
let nodeTag = ['TITLE', 'H1', 'H2', 'H3', 'H4', 'H5', 'H6', 'P', 'A',  'UL', 'OL', 'LI', 'TABLE', 'TR', 'TD', 'FORM', 'OPTION', 'BUTTON', 'DIV', 'SPAN',"EM",'MAIN','HEADER','NAV','ARTICLE','EECTION','ASIDE','FOOTER','FIGURE','GIGCAPTION',"SECTION"]
// 将Vue APP插入刚创建的di
function annotateTextNodes(nodes, keywords) {
  nodes.forEach((node) => {
    if (node && node.textContent&&node.nodeType === Node.TEXT_NODE) {
      for (const keyword of keywords) {
        const regex = new RegExp(`\\b(${keyword})\\b`, "gi");
        const replacement = `<span class="aodt-keywords" data-keyWords="$1">$1</span>`;
        let regex1 = /^0x([0-9a-fA-F]{40})$/;  // 正则表达式  
        let match = regex1.test(node.textContent);  // 使用正则表达式测试字符串  
        let text =  node.textContent.replace(/^0x([0-9a-fA-F]{40})$/, `<span class="aodt-keywords" data-keyWords="0x$1">0x$1</span>`);
        const replacedText = text.replace(regex, replacement);
        if (replacedText !== node.textContent) {
          const newNode = document.createElement("span");
          newNode.innerHTML = replacedText;
          node.replaceWith(newNode);
          break;
        }
      }
    } else if (
      node.nodeType === Node.ELEMENT_NODE &&
      nodeTag.includes(node.nodeName)&&
      node.id !== "adot-container"&&
      node.id !== "aodt-keywords-popup"&&
      node.firstChild
      ) {
      annotateTextNodes(node.childNodes, keywords);
    }
  });
}
//添加特定样式声明
function addDropOffStyle() {
  const style = document.createElement("style");

  style.setAttribute("data-h", "mime");
  style.innerHTML =
    ".aodt-keywords {text-decoration: rgb(0,181,217) dashed underline;text-underline-offset: 4px;display: inline!important;position: relative;} .adot-tip{display:none}";
  document.head.appendChild(style);
}

// content_script.js
let keywordsData = []
chrome.runtime.sendMessage({ msg: "GETDATA" }, (res) => {
  clearInterval(myTimer);
  num = 0
   keywordsData = res
  myTimer = setInterval(myInterval, 500);
});

chrome.runtime.onMessage.addListener(function (message, sender, sendResponse) {
  // 在这里处理接收到的消息
  if (message.action === "ReLoaded") {
     keywordsData = message.data
    clearInterval(myTimer);
    num = 0
    myTimer = setInterval(myInterval, 500);
  }
});

chrome.runtime.onMessage.addListener(function (message, sender, sendResponse) {
  // 在这里处理接收到的消息
  if (message.action === "pageLoaded") {
     keywordsData = message.data
    clearInterval(myTimer);
    num = 0
    myTimer = setInterval(myInterval, 500);
  }
});

let innerText = ""
function myInterval() {
  num+=1
  if(num>60){
    clearInterval(myTimer);
  }
  if(num==0){
    innerText = ""
  }
  if(innerText==document.body.innerText){
    let newKeywordsData = [];
    keywordsData.map((data) => {
       if (innerText.toLocaleLowerCase().indexOf(data) > 0) {
         newKeywordsData.push(data);
        }
      });
      console.time();
      annotateTextNodes(document.body.childNodes, newKeywordsData);
      console.timeEnd();
      clearInterval(myTimer);
  }
  innerText = document.body.innerText;
  addDropOffStyle();
}