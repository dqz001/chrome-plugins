import { createRouter, createWebHistory } from 'vue-router'
import Home from '@/popup/views/home/index.vue' 
import Search from '@/popup/views/search/index.vue'

const routes = [
    {
        path: '/',
        component: Home,
    },
    {
      path: '/search',
      component: Search,
  },
]

const router = createRouter({
    history: createWebHistory(),
    routes,
})

export default router