export default {
  'home.about': 'About',
  'google.plugin.title': 'Add to Chrome',
  'login.title': 'Login',
  'loginout.title': 'Log Out',
  'ai.content.credits.befort': '',
  'ai.content.credits.after': 'left today',
  'ai.chat.try.again': 'Sorry, the knowledge is not found, please try again later',
  'ai.chat.limit': 'Your usage has reached its limit, please try again tomorrow',
  'ai.chat.useful': 'Useful',
  'ai.chat.useless': 'Useless',
  'ai.login.sign': 'Sign in or sign up to continue',
  'ai.login.connect': 'Connect your wallet',
  'ai.login.web2': 'Sign in with web2',
  'ai.login.agreetext.befort': 'By clicking on any of the above login methods, you acknowledge that you have read and understood, and agree to our',
  'ai.login.agreetext.after': 'and',
  'ai.login.agreetext.service': 'Terms of Service',
  'ai.login.agreetext.privacy ': 'Privacy Policy',
  'ai.login.invitation.code': 'Enter the invitation code',
  'ai.login.invitation.placeholder': 'Please enter the invitation code',
  'ai.login.submit': 'Confirm',
  'ai.login.cancel': 'Cancel',
  'ai.login.success': 'Login success',
  'ai.chat.sources': 'Sources',
  'ai.chat.explain.more':'explain more',
  'ai.chat.explain.visit':'visit',
  'web3.back': 'Back',
  'web3.metaMsak': 'Connect your MetaMask wallet',
  'web3.temple': 'Connect your Temple wallet',
  'web3.okx': 'Connect your OKX wallet',
};
