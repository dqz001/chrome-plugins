import { createI18n } from "vue-i18n";
import ZH_CN from "./ZH-CN.js";
import ZH_TW from "./ZH-TW.js";
import EN from "./EN.js";
import RU from "./RU.js";
import HI from "./HI.js";
import ID from "./ID.js";
import DE from "./DE.js";
import ES from "./ES.js";
import JA from "./JA.js";
import KO from "./KO.js";
import DEFAULT from "./DEFAULT.js";
import { useStore } from "vuex";
import store from '@/store'

// 如果本地缓存记录了语言环境，则使用本地缓存
export default createI18n({
  locale:store.state?.language?.symbol || "EN",
  allowComposition: true,
  legacy: false, // 设为true或者不设置
  messages: {
    ZH_CN,
    ZH_TW,
    EN,
    RU,
    HI,
    ID,
    DE,
    ES,
    JA,
    KO,
    DEFAULT
  },
});

export const langs = [
  {
    name: "language",
    symbol: "DEFAULT",
  },
  {
    name: "简体中文",
    symbol: "ZH_CN",
  },
  {
    name: "繁体中文",
    symbol: "ZH_TW",
  },
  {
    name: "English",
    symbol: "EN",
  },
  {
    name: "Pусский",
    symbol: "RU",
  },
  {
    name: "हिंदी",
    symbol: "HI",
  },
  {
    name: "Bahasa indonesia",
    symbol: "ID",
  },
  {
    name: "Deutsch",
    symbol: "DE",
  },
  {
    name: "Español",
    symbol: "ES",
  },
  {
    name: "日本語",
    symbol: "JA",
  },
  {
    name: "한국어",
    symbol: "KO",
  },
];
