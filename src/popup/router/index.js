import { createRouter, createWebHashHistory } from 'vue-router'
const routes = [
    {
        path: '/',
        component: () => import("@/popup/views/home/index.vue"),
    },
    {
      path: '/search',
      component: () => import("@/popup/views/search/index.vue"),
    },
    {
      path: '/loopSearch',
      component: () => import("@/content/components/loopSearch/index.vue"),
    },
]

const router = createRouter({
    history: createWebHashHistory(),
    routes,
})

export default router