import { createStore } from "vuex";
import createPersistedState from 'vuex-persistedstate'
import VuexChromePlugin from 'vuex-chrome-plugin'
export default createStore({
	state: {
		searchData: "",
		pageNum: 1,
    siteTabId:2,
    isLogin:false,
    isInvitation:false,
    language:{
      name: "language",
      symbol: "DEFAULT",
    },
	  againSearchText:'',
    site:'',
	},
	mutations: {
		setSearchData(state, data) {
			state.searchData = data;
		},
    setLogin(state, data) {
			state.isLogin = data;
		},
    setSite(state, data) {
      state.site = data;
    },
    setTabSite(state, data) {
      state.siteTabId = data;
    },
    setInvitationCode(state, data) {
			state.isInvitation = data;
		},
    setLanguage(state, data) {
			state.language = data;
      if(chrome&&chrome.storage&&chrome.storage.sync){
        chrome.storage.sync.set({language: data })
      }
		},
    againSearch(state,data){
      state.againSearchText = data
    }
	},
	actions: {},
	modules: {},
  plugins: [  
    // createPersistedState({
    //     storage: localStorage,
    // }) 
    // VuexChromePlugin()
],
});
